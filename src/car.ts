import {IGameCharacter} from './IGameCharacter';

export class Car implements IGameCharacter {
    renderer: any;
    width = 40;
    height = 20;
    sprite: PIXI.Sprite;
    textureCar: PIXI.Texture;
    v: number;
    direct: number;
    constructor(renderer: any) {
        this.renderer = renderer.renderer;
        this.textureCar = PIXI.Texture.fromImage('assets/car1_spr.png');
    }
    createCar(row: any, x: any, v: number, direct: number) {
        this.v = v;
        this.direct = direct;
        let car = new PIXI.Sprite(this.textureCar);
        car.width = this.width;
        car.height = this.height;
        car.position.set(x, row);
        this.sprite = car;
        return car;
    }
    setCarPosition() {
        if (this.direct === -1) {
            this.sprite.x = this.renderer.width;
        } else {
            this.sprite.x = 0;
        }
    }
    gameLoop(delta: any) {
        this.sprite.x += this.v * this.direct;
        this.checkBounds();
    }
    checkBounds() {
        if ((this.sprite.x + this.sprite.width) < 0 || this.sprite.x > this.renderer.width) {
            this.setCarPosition();
        }
    }
}