import {IGameCharacter} from './IGameCharacter';
export class Log implements IGameCharacter{
    renderer: any;
    width = 70;
    height = 20;
    sprite: PIXI.Sprite;
    textureLog: PIXI.Texture;
    v: number;
    direct: number;
    constructor(renderer: any) {
        this.renderer = renderer.renderer;
        this.textureLog = PIXI.Texture.fromImage('assets/container001-red-small.png');
    }
    createLog(row: any, x: any, v: number, direct: number) {
        this.v = v;
        this.direct = direct;
        let log = new PIXI.Sprite(this.textureLog);
        log.width = this.width;
        log.height = this.height;
        log.position.set(x, row);
        this.sprite = log;
        return log;
    }
    setLogPosition() {
        // this.sprite.x = 0 - this.sprite.width;
        if (this.direct === -1) {
            this.sprite.x = this.renderer.width;
        } else {
            this.sprite.x = -70;
        }
    }
    gameLoop(delta: any) {
        this.sprite.x += this.v * this.direct;;
        this.checkBounds();
    }
    checkBounds() {
        if ((this.sprite.x  > this.renderer.width) || (this.sprite.x + this.sprite.width) < 0) {
            this.setLogPosition();
        }
    }
}