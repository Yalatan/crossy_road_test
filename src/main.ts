import { Game } from './game';
import * as PIXI from 'pixi.js';

window.onload = function () {
    let renderer = new PIXI.Application(600, 390);
    document.body.appendChild(renderer.view);
    let game = new Game(renderer);
};