import {IGameCharacter} from './IGameCharacter';

export class Player  implements IGameCharacter {
    renderer: any;
    texturePlayer: any;
    width = 30;
    height = 25;
    sprite: PIXI.Sprite;
    constructor(renderer: any) {
        this.renderer = renderer.renderer;
        this.texturePlayer = PIXI.Texture.fromImage('assets/blackBugAlien.png');
    }
    createPlayer () {
        let playerSprite = new PIXI.Sprite(this.texturePlayer);
        playerSprite.width = this.width;
        playerSprite.height = this.height;
        playerSprite.position.set(300, 360);
        this.sprite = playerSprite
        return playerSprite;
    }
}