import * as PIXI from 'pixi.js';
import Graphics = PIXI.Graphics;
import {Car} from './car';
import {Log} from './log';
import Container = PIXI.Container;
import {IGameCharacter} from './IGameCharacter';
import {Player} from './player';

export class Game {
    renderer: any;
    container: any;
    rows = [35, 65, 95, 125, 155, 215, 245, 275, 305, 335, 365];
    key: any;
    car: any;
    logs: Array<Log> = new Array<Log>();
    player: any;
    context: any;
    controls:any = {};

    constructor(renderer: any) {
        this.renderer = renderer;
        this.drawBackground();
        this.setup();

    }
    drawBackground() {
        let rectangleAqua = new Graphics();
        rectangleAqua.beginFill(0x1099bb);
        rectangleAqua.drawRect(0, 30, 600, 150);
        rectangleAqua.endFill();
        this.renderer.stage.addChild(rectangleAqua);
        let rectangleRoad = new Graphics();
        rectangleRoad.beginFill(0x543500);
        rectangleRoad.drawRect(0, 210, 600, 150);
        rectangleRoad.endFill();
        this.renderer.stage.addChild(rectangleRoad);
        let rectangleGrass = new Graphics();
        rectangleGrass.beginFill(0x008000);
        rectangleGrass.drawRect(0, 0, 600, 30);
        rectangleGrass.drawRect(0, 180, 600, 30);
        rectangleGrass.drawRect(0, 360, 600, 30);
        rectangleGrass.endFill();
        this.renderer.stage.addChild(rectangleGrass);
    }

    loader() {
        PIXI.loader
            .add(['assets/car1_spr.png',
                'assets/container001-red-small.png',
                'assets/bush.png'])
            .load(this.setup);
    }

    mover(ctx: any, step: number) {
        ctx.controls.left = ctx.keyboard(37);
        ctx.controls.left.press = () => {
            ctx.player.position.set(ctx.player.position.x - step, ctx.player.position.y);
        };
        ctx.controls.right = ctx.keyboard(39);
        ctx.controls.right.press = () => {
            ctx.player.position.set(ctx.player.position.x + step, ctx.player.position.y);
        };
        ctx.controls.up = ctx.keyboard(38);
        ctx.controls.up.press = () => {
            ctx.player.position.set(ctx.player.position.x, ctx.player.position.y - step);
            this.checkPlayerBounds(ctx.player);
        };
        ctx.controls.down = ctx.keyboard(40);
        ctx.controls.down.press = () => {
            ctx.player.position.set(ctx.player.position.x, ctx.player.position.y + step);
            this.checkPlayerBounds(ctx.player);
        };
    }

    setup() {
        this.container = new PIXI.Container();
        this.renderer.stage.addChild(this.container);
        this.makeCars();
        this.makeLogs();
        this.makePlayer();
        // this.renderer.ticker.add(delta => this.gameLoop(delta));
        this.mover(this, 30);

    }
    makeCars() {
            this.makeCar(this.rows[5], 0, 1, -1);
            this.makeCar(this.rows[5], 300, 1, -1);
            this.makeCar(this.rows[5], 430, 1, -1);
            this.makeCar(this.rows[6], 10, 0.7, -1);
            this.makeCar(this.rows[7], 370, 1.2, 1);
            this.makeCar(this.rows[7], 150, 1.2, 1);
            this.makeCar(this.rows[8], 200, 0.9, -1);
            this.makeCar(this.rows[8], 520, 0.9, -1);
            this.makeCar(this.rows[9], 30, 1.2, 1);
            this.makeCar(this.rows[9], 490, 1.2, 1);
    }
    makeCar(row: any, x: number, v: number, direct: number) {
        let car: Car = new Car(this.renderer);
        let result = car.createCar(row, x, v, direct);
        this.container.addChild(result);
        this.play(car);
        return car;
    }
    makeLogs() {
        let logs = this.logs
        logs.push(this.makeLog(this.rows[4], 100, 1, 1))
        logs.push(this.makeLog(this.rows[3], 300, 1.3, -1));
        logs.push(this.makeLog(this.rows[3], 130, 1.3, -1));
        logs.push(this.makeLog(this.rows[3], 10, 1.3, -1));
        logs.push(this.makeLog(this.rows[2], 10, 1, 1));
        logs.push(this.makeLog(this.rows[2], 150, 1, 1));
        logs.push(this.makeLog(this.rows[2], 300, 1, 1));
        logs.push(this.makeLog(this.rows[1], 520, 0.8, -1));
        logs.push(this.makeLog(this.rows[1], 320, 0.8, -1));
        logs.push(this.makeLog(this.rows[0], 170, 1, 1));
        logs.push(this.makeLog(this.rows[0], 390, 1, 1));
    }
    makeLog(row: any, x: number, v: number, direct: number) {
        let log: Log = new Log(this.renderer);
        let result = log.createLog(row, x, v, direct);
        this.container.addChild(result);
        this.play(log);
        return log;
    }
    makePlayer() {
        let player: Player = new Player(this.renderer);
        let result = player.createPlayer();
        //
        // let texture = PIXI.Texture.fromImage('assets/blackBugAlien.png');
        // this.player = new PIXI.Sprite(texture);
        // this.player.width = 30;
        // this.player.height = 25;
        // this.player.position.set(300, 360);
        this.container.addChild(result);
        this.player = result;
        return player;
        // return this.player;
    }
    checkPlayerBounds(player: any) {
        if (player.position.y < 0 || player.position.y > 360) {
            player.y = 360;
        }
    }
    keyboard(keyCode: any) {
        let key: any = {};
        key.code = keyCode;
        key.isDown = false;
        key.isUp = true;
        key.press = undefined;
        key.release = undefined;
               // The `downHandler`
        key.downHandler = (event: any) => {
            if (event.keyCode === key.code) {
                if (key.isUp && key.press) key.press();
                key.isDown = true;
                key.isUp = false;
            }
            event.preventDefault();
        };

        // The `upHandler`
        key.upHandler = (event: any) => {
            if (event.keyCode === key.code) {
                if (key.isDown && key.release) key.release();
                key.isDown = false;
                key.isUp = true;
            }
            event.preventDefault();
        };

        // Attach event listeners
        window.addEventListener(
            'keydown', key.downHandler.bind(key), false
        );
        window.addEventListener(
            'keyup', key.upHandler.bind(key), false
        );
        return key;
    }
    hitSprites(r1: any, r2: any) {

        // Define the variables we'll need to calculate
        let hit, combinedHalfWidths, combinedHalfHeights, vx, vy;

        // hit will determine whether there's a collision
        hit = false;

        // Find the center points of each sprite
        r1.centerX = r1.x + r1.width / 2;
        r1.centerY = r1.y + r1.height / 2;
        r2.centerX = r2.x + r2.width / 2;
        r2.centerY = r2.y + r2.height / 2;

        // Find the half-widths and half-heights of each sprite
        r1.halfWidth = r1.width / 2;
        r1.halfHeight = r1.height / 2;
        r2.halfWidth = r2.width / 2;
        r2.halfHeight = r2.height / 2;

        // Calculate the distance vector between the sprites
        vx = r1.centerX - r2.centerX;
        vy = r1.centerY - r2.centerY;

        // Figure out the combined half-widths and half-heights
        combinedHalfWidths = r1.halfWidth + r2.halfWidth;
        combinedHalfHeights = r1.halfHeight + r2.halfHeight;

        // Check for a collision on the x axis
        if (Math.abs(vx) < combinedHalfWidths) {

            // A collision might be occurring. Check for a collision on the y axis
            if (Math.abs(vy) < combinedHalfHeights) {

                // There's definitely a collision happening
                hit = true;
            } else {

                // There's no collision on the y axis
                hit = false;
            }
        } else {

            // There's no collision on the x axis
            hit = false;
        }

        // `hit` will be either `true` or `false`
        return hit;
    }
    play(obj: any) {
         this.renderer.ticker.add((delta: any) => {
             obj.gameLoop(delta);
             if (this.player) {
                 this.checkCollision(obj, this.player);
             }
         });
    }


    checkCollision(gameCharacter: IGameCharacter, player: any) {
        // if (this.hitSprites(gameCharacter.sprite, player)) {
        //     if (!gameCharacter.inverseCheck) {
        //         player.position.set(300, 360);
        //     } else {
        //         console.log('inverse');
        //     }
        // } else {
        //     if (gameCharacter.inverseCheck && (30 < player.y) && (player.y < 180)) {
        //         console.log('win');
        //     } else {
        //         player.position.set(300, 360);
        //     }
        // }
        if ((210 <= player.y) && (player.y < 360)) {
            if (this.hitSprites(gameCharacter.sprite, player)) {
                player.position.set(300, 360);
            }
        }
       if ((30 < player.y) && (player.y < 180)) {
           let res = this.logs.filter(log => {
               return  this.hitSprites(log.sprite, player);
           })
           if (res.length > 0) {
               console.log('on log', res);
           } else {
               player.position.set(300, 360);
           }
       }


                     // console.log('inverse');
                 // }
            // } else {
            //     if (gameCharacter.inverseCheck && (30 < player.y) && (player.y < 180)) {
            //         console.log('win');
            //     } else {
            //         player.position.set(300, 360);
            //     }

    }
}